import { createStore } from "vuex";
import createPersistedState from "vuex-persistedstate";
import { getData, postFolder, reload } from "@/api/getFiles";

export default createStore({
  state: {
    photos: [],
    folder: "",
  },
  getters: {
    photos: (s) => s.photos,
    folder: (s) => s.folder,
  },
  mutations: {
    SET_PHOTOS(state, photos) {
      state.photos = photos;
    },
    SET_FOLDER(state, folder) {
      state.folder = folder;
    },
    RELOAD_PHOTO(state, photo) {
      state.photos = [...state.photos, photo];
    },
    REMOVE_PHOTO(state, photo) {
      console.log(
        state.photos.filter((e) => {
          return e.split("/").reverse()[0].toLocaleLowerCase() !== photo;
        })
      );
      state.photos = state.photos.filter((e) => {
        return e.split("/").reverse()[0].toLocaleLowerCase() !== photo;
      });

      console.log(state.photos);
    },
    RESET_PHOTOS(state) {
      state.photos = [];
      state.folder = "";
    },
  },
  actions: {
    async getPhotos({ commit }) {
      const response = await getData();

      const photos = response.data;
      commit("SET_PHOTOS", photos);
    },
    async postFolder({ commit }, folder) {
      const response = await postFolder(folder);

      const photos = response.data;
      commit("SET_PHOTOS", photos);
      commit("SET_FOLDER", folder);
    },
    async reloadPhoto({ commit }, { folder, file }) {
      const response = await reload(folder, file);
      const newFileName = response?.data.split("/").reverse()[0].toLocaleLowerCase();
      commit("REMOVE_PHOTO", file);
      commit("RELOAD_PHOTO", newFileName);
    },
    async removePhoto({ commit }, { file }) {
      commit("REMOVE_PHOTO", file);
    },
  },
  modules: {},
  plugins: [createPersistedState()],
});
