import axios from "axios";

const getData = async () => {
  try {
    return await axios.get("http://localhost:3001/list");
  } catch (e) {
    console.error(e);
  }
};

const postFolder = async (folder) => {
  try {
    console.log(folder);
    return await axios.post("http://localhost:3001/list", { folder });
  } catch (e) {
    console.error(e);
  }
};

const reload = async (folder, file) => {
  try {
    return await axios.put("http://localhost:3001/reload", { folder, file });
  } catch (e) {
    console.error(e);
  }
};
const remove = async () => {
  try {
    return await axios.delete("http://localhost:3001/remove");
  } catch (e) {
    console.error(e);
  }
};
export { getData, postFolder, reload, remove };
